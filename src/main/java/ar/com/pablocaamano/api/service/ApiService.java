package ar.com.pablocaamano.api.service;

import ar.com.pablocaamano.api.model.DTO.ApiObjectDTO;

import java.util.List;

public interface ApiService {

    ApiObjectDTO findObject(String id);
    List<ApiObjectDTO> findAllObjects();
    ApiObjectDTO insertObject(ApiObjectDTO object);
    ApiObjectDTO deleteObject(String id);
    ApiObjectDTO updateObject(ApiObjectDTO apiObject);
}
