package ar.com.pablocaamano.api.service;

import ar.com.pablocaamano.api.model.ApiObject;
import ar.com.pablocaamano.api.model.DTO.ApiObjectDTO;
import ar.com.pablocaamano.api.repository.ApiRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ApiServiceImpl implements ApiService{

    @Autowired
    private ApiRepository repository;

    private ModelMapper modelMapper;

    public ApiServiceImpl(){
        this.modelMapper = new ModelMapper();
    }

    @Override
    public ApiObjectDTO findObject(String id) {
        ApiObject objectFinded = repository.findOne(id);
        if (objectFinded != null){
            ApiObjectDTO apiObjectDTO  = modelMapper.map(objectFinded,ApiObjectDTO.class);
            return apiObjectDTO;
        }else {
            return null;
        }
    }

    @Override
    public List<ApiObjectDTO> findAllObjects() {
        List<ApiObject> objectList = repository.findAll();
        if (objectList != null && !objectList.isEmpty()){
            List<ApiObjectDTO>dtoList = new ArrayList<ApiObjectDTO>();
            for (ApiObject apiObject: objectList){
                ApiObjectDTO objectDTO = modelMapper.map(apiObject,ApiObjectDTO.class);
                dtoList.add(objectDTO);
            }
            return dtoList;
        }else{
            return null;
        }
    }

    @Override
    public ApiObjectDTO insertObject(ApiObjectDTO object){
        ApiObject objectFinded = repository.findOne(object.getObjectId());
        if (objectFinded == null ) {
            ApiObject apiObject = modelMapper.map(object,ApiObject.class);
            ApiObject objectInserted = repository.insert(apiObject);
            return modelMapper.map(objectInserted,ApiObjectDTO.class);
        }else{
            return null;
        }
    }

    @Override
    public ApiObjectDTO deleteObject(String idObject){
        ApiObject objectFinded = repository.findOne(idObject);
        if (objectFinded != null) {
            repository.delete(objectFinded.getId());
            return modelMapper.map(objectFinded,ApiObjectDTO.class);
        }else{
            return null;
        }
    }

    @Override
    public ApiObjectDTO updateObject(ApiObjectDTO apiObject) {
        return null;
    }
}
