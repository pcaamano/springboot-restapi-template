package ar.com.pablocaamano.api.exception;

import org.springframework.http.HttpStatus;

public class ErrorMappingModelException extends RuntimeException implements PersonalizedHttpException {

    private HttpStatus httpStatus;
    private String message;

    public ErrorMappingModelException(HttpStatus httpStatus){
        super();
        this.httpStatus = httpStatus;
    }

    public ErrorMappingModelException(HttpStatus httpStatus, String message){
        super(message);
        this.httpStatus = httpStatus;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
