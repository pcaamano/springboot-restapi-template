package ar.com.pablocaamano.api.exception;

import org.springframework.http.HttpStatus;

public interface PersonalizedHttpException {
    HttpStatus getHttpStatus();
}
