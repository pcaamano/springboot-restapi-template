package ar.com.pablocaamano.api.controller;

import ar.com.pablocaamano.api.model.ApiResponse;
import ar.com.pablocaamano.api.model.DTO.ApiObjectDTO;
import ar.com.pablocaamano.api.model.Error;
import ar.com.pablocaamano.api.model.Meta;
import ar.com.pablocaamano.api.service.ApiService;
import ar.com.pablocaamano.api.model.ApiObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private ApiService apiService;

    private Logger logger = LoggerFactory.getLogger(ApiController.class);

    private static final String ERROR_400_CODE = "ERR-400";
    private static final String ERROR_400_DESCRIPTION = "Bad request.";
    private static final String ERROR_404_CODE = "ERR-404";
    private static final String ERROR_404_DESCRIPTION = "Results not found for request.";
    private static final String ERROR_500_CODE = "ERR-500";

    public ApiResponse generateResponse(String method, String operation, List<ApiObjectDTO>objectList, String errorCode, String errorMessage){
        ApiResponse response = new ApiResponse();
        Meta meta = new Meta();
        meta.setMethod(method);
        meta.setOperation(operation);
        response.setMeta(meta);
        if(errorCode==null && errorMessage==null){
            response.setData(objectList);
            response.setErrors(Collections.emptyList());
            return response;
        }else {
            response.setData(Collections.emptyList());
            List<Error> errorList = new ArrayList<Error>();
            Error error = new Error();
            error.setCode(errorCode);
            error.setMessage(errorMessage);
            errorList.add(error);
            response.setErrors(errorList);
            return response;
        }
    }

    /**
     * GET object by id
     **/
    @GetMapping(value = "get/{id}")
    @ResponseBody
    public ResponseEntity<ApiResponse> getObjectBy(@PathVariable String id){
        String method = String.valueOf(HttpMethod.GET);
        String operation = "/api/get/" + id;
        logger.info(method.concat("_").concat(operation));
        try {
            ApiObjectDTO apiObjectReturned = apiService.findObject(id);
            if(apiObjectReturned != null){
                logger.info("Query success.");
                List<ApiObjectDTO> objectsList = new ArrayList<ApiObjectDTO>();
                objectsList.add(apiObjectReturned);
                return new ResponseEntity<>(
                        generateResponse(method,operation,objectsList,null,null)
                        ,HttpStatus.OK);
            }else{
                logger.info(ERROR_404_DESCRIPTION);
                return new ResponseEntity<>(
                        generateResponse(method,operation,null,ERROR_404_CODE, ERROR_404_DESCRIPTION)
                        ,HttpStatus.NOT_FOUND);
            }
        }catch (Exception exception){
            logger.error(exception.getMessage());
            return new ResponseEntity<>(
                    generateResponse(method,operation,null,ERROR_500_CODE, exception.getMessage())
                    ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * GET all objects
     **/
    @GetMapping(value = "/get/all")
    @ResponseBody
    public ResponseEntity<ApiResponse> getAllObjects(){
        String method = String.valueOf(HttpMethod.GET);
        String operation = "/api/get/all";
        logger.info(method.concat("_").concat(operation));
        try{
            List<ApiObjectDTO> objectsList = apiService.findAllObjects();
            if (objectsList==null && objectsList.isEmpty()){
                logger.info("Objects listed.");
                return new ResponseEntity<>(
                        generateResponse(method,operation,null,ERROR_404_CODE,ERROR_404_DESCRIPTION)
                        ,HttpStatus.NOT_FOUND);
            }else{
                logger.info(ERROR_404_DESCRIPTION);
                return new ResponseEntity<>(
                        generateResponse(method,operation,objectsList,null,null)
                        ,HttpStatus.OK);
            }
        }catch (Exception exception){
            logger.error(exception.getMessage());
            return new ResponseEntity<>(
                    generateResponse(method,operation,null,ERROR_500_CODE,exception.getMessage())
                    ,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * INSERT object
     */
    @PostMapping(value = "/insert")
    public ResponseEntity<ApiResponse> insertObject(@RequestBody ApiObjectDTO object){
        String method = String.valueOf(HttpMethod.POST);
        String operation = "/api/insert";
        logger.info(method.concat("_").concat(operation));
        try {
            ApiObjectDTO apiObjectReturned = apiService.insertObject(object);
            if (apiObjectReturned != null) {
                logger.info("Object with Id ".concat(object.getObjectId()).concat(" success insert."));
                List<ApiObjectDTO> objectsList = new ArrayList<>();
                objectsList.add(apiObjectReturned);
                return new ResponseEntity<>(
                        generateResponse(method,operation,objectsList, null, null),
                        HttpStatus.OK);
            } else {
                logger.info(ERROR_400_DESCRIPTION);
                return new ResponseEntity<>(
                        generateResponse(method,operation,null,ERROR_400_CODE,ERROR_400_DESCRIPTION),
                        HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exception){
            logger.error(exception.getMessage());
            return new ResponseEntity<>(
                    generateResponse(method,operation,null,ERROR_500_CODE,exception.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * DELETE object by id
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> deleteObject(@PathVariable String id){
        String method = String.valueOf(HttpMethod.DELETE);
        String operation = "/api/delete/" + id;
        logger.info(method.concat("_").concat(operation));
        try {
            ApiObjectDTO apiObjectReturned = apiService.deleteObject(id);
            if (apiObjectReturned != null) {
                logger.info("Object id ".concat(apiObjectReturned.getObjectId()).concat(" sucess delete."));
                List<ApiObjectDTO> objectsList = new ArrayList<>();
                objectsList.add(apiObjectReturned);
                return new ResponseEntity<>(
                        generateResponse(method,operation,objectsList,null,null),
                        HttpStatus.OK);
            } else {
                logger.info(ERROR_404_DESCRIPTION);
                return new ResponseEntity<>(
                        generateResponse(method,operation,null,ERROR_404_CODE,ERROR_404_DESCRIPTION),
                        HttpStatus.NOT_FOUND);
            }
        }catch (Exception exception){
            logger.error(exception.getMessage());
            return new ResponseEntity<>(
                    generateResponse(method,operation,null,ERROR_500_CODE,exception.getMessage()),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
