package ar.com.pablocaamano.api.model;

import ar.com.pablocaamano.api.model.DTO.ApiObjectDTO;

import java.util.List;

public class ApiResponse {
    private Meta meta;
    private List<ApiObjectDTO>data;
    private List<Error> errors;

    public List<ApiObjectDTO> getData() {
        return data;
    }

    public void setData(List<ApiObjectDTO> data) {
        this.data = data;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error>errorList) {
        this.errors = errorList;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
