package ar.com.pablocaamano.api.model.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * model to map json request
 */
public class ApiObjectDTO {
    @JsonProperty(value = "object_id")
    private String objectId;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "number")
    private String number;

    public String getName() {
        return name;
    }

    public void setId(String objectId){
        this.objectId = objectId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}