package ar.com.pablocaamano.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Mongo model
 **/
@Document(collection = "testing")
@CompoundIndexes({@CompoundIndex(name = "index",unique = true,def = "{'id_object' : 1}")})
public class ApiObject {
    @Id
    @JsonIgnore
    private String id;

    @Field(value = "id_object")
    private String idObject;

    @Field(value = "name")
    private String name;

    @Field(value = "number")
    private String number;

    public ApiObject(){}

    public ApiObject(String idObject){
        setIdObject(idObject);
        setName("test"+id);
        setNumber("test"+id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdObject() {
        return idObject;
    }

    public void setIdObject(String idObject) {
        this.idObject = idObject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
