package ar.com.pablocaamano.api.repository;

import ar.com.pablocaamano.api.model.ApiObject;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface ApiRepository extends MongoRepository<ApiObject, Integer> {

    ApiObject findOne(String id);
    List<ApiObject> findAll();
    ApiObject insert(ApiObject apiObject);
    void delete(String id);
    ApiObject save(ApiObject apiObject);
}

